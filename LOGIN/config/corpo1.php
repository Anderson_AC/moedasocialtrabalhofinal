<?php
 include ("conn.php");



//cria a instrução SQL que vai selecionar os dados
$query = sprintf("SELECT nome, saldo FROM dadosusuario ");

//executa a query
$dados = mysqli_query($conn, $query) or die(mysqli_error());

//transforma os dados em uma lista
$linha = mysqli_fetch_assoc($dados);


?>
<html>
  <head><title>Pagina inicial</title>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
    <script type="text/javascript" src="http://google.com.br"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.2.1.min.js">
    </script>
    <meta name="text:Search Label" content="" />
    <link href="config/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
  </head>
  <body>
    <div class="col-md-12">
      <div class="col-md-8">
        <div class="col-md-4">   
          <p>
            <font color="white" face="Arial" > Seja bem vindo 
            <?=$linha['nome']?> 
            , saldo: 
            <?=$linha['saldo']?>
            </font>
          </p>
        </div>
      </div>
    </div>
    <div id="tudo">
      <div id="cabecalho">
        <h2>Moeda Social Digital</h2>
      </div>
    </div>
  

    <!-- Formulario da caixa de pesquisa -->
    <form onsubmit="return tagSearch(this)">
      <input type="text" name="tag" value="Pesquisar" 
       onfocus="if (this.value == '{text:Search Label}') {
       this.value=''
       }" 
       onblur="if (this.value == '') {
       this.value='{text:Search Label}'
       }" />
      <input type="submit" value="Pesquisar" />
    </form>
    <div class="principal">    
      <div class="container">
        <div class="row">
          <div id="corpo" class="col-md-12">
            <div  class="col-md-4">
           	  <div id="menu">
                <ul id="nav">
                  <li>
                    <a href="login.php">Sair</a>
                  </li>
                  <li>
                    <a href="fundada2.php">Sobre</a>
                  </li>
                  <li>
                    <a href="ideia2.php">Ideia</a>
                  </li>
                  <li> 
                    <a href="contato2.php">Contato</a>
                  </li>
                  <li>
                    <a href="procurarServico.php">Ofertas</a>
                  </li>
                  <li>
                    <a href="pagamento.php">Pagamentos</a>
                  </li>
                  <li>
                    <a href="anunciosconveniados.php">Anuncios de Conveniados</a>
                  </li>
                  <li>
                    <a href="lojasconveniadas.php">Lojas</a>
                  </li>
                  <li><a href="corpo1.php">Cadastros</a>
                    <ul>
                      <li>
                        <a href="cadastroServico.php">Serviços</a>
                      </li>
                      <li>
                        <a href="cadastromaodeobra.php">Mão de obra</a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
              <div id="myCarousel" class="carousel  slide"  data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                  <li data-target="#myCarousel" data-slide-to="3"></li>
                   <li data-target="#myCarousel" data-slide-to="4"></li>
                </ol>
                
                <div class="carousel-inner" role="listbox">
                  <div class="item active">
                    <img src="imagem/guia.jpg" alt="Guia das Comunidades">
                  </div>

                  <div class="item">
                    <img src="imagem/mulher.jpg" alt="Evento Feminino">
                  </div>
                  <div class="item active">
                    <img src="imagem/cal.jpg" alt="Calendario">
                  </div>        
                  <div class="item active">
                    <img src="imagem/eve.jpg" alt="Centro de Eventos">
                  </div>         
                </div>
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Anterior</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Próxima</span>
                  </a>
                </div>
            </div>
            </div>            
  				</div>
        </div>
      </div>
    </div>
  </body>  
</html>
