 <!doctype html>
 <html>
 <head><title>Fornecer Serviços</title>
      
      <link href="fornecerServico.css" rel="stylesheet"> 
      <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    

 </head>
 <body>   
    
     <div class="container">   

     <div class="row">  
     <div id="tudo">
      <div id="cabecalho">
        <h2>Cadastro de Serviços</h2>
      </div>
  </div>
           <div class="col-md-12 corLink">
               <div id="menu">
    <ul id="nav">
        <li><a href="paginainicial.php">Home</a></li>
        <li><a href="fundada2.php">Sobre</a></li>
        <li><a href="ideia2.php">Ideia</a>
        <li><a href="contato2.php">Contato</a></li>
        
    </ul>
  </div>
         </div>
   <div class="principal">    
     <div class="container">
       <div class="row">
         <div id="corpo"class="col-md-12">
            <div class="col-md-2">
            </div>

            <div class="col-md-8"> 
          	  
              <div id="corpo">
			    <form class="form-horizontal" method="post" action="banco/cadastroServico.php">
                  <div class="form-group">
                    <label  class="col-sm-3 control-label">Codigo Serviços</label>
       
                     <div class="col-sm-2">
                       <input type="text" class="form-control" name="codigoServico" placeholder="Código">
                     </div>

                     <label  class="col-sm-2 control-label">Data Cadastro</label>
       
                     <div class="col-sm-5">
                       <div class="col-sm-4">
                         <input type="text" class="form-control" name="diaServico" placeholder="Dia">
                       </div>

                       <div class="col-sm-4">
                         <input type="text" class="form-control" name="mesServico" placeholder="Mês">
                       </div>

                       <div class="col-sm-4">
                         <input type="text" class="form-control" name="anoServico" placeholder="Ano">
                       </div>
                     </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Nome Da Empresa</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="empresaServico" placeholder="Nome Serviço">
                  </div>
                  </div> 

                  <div class="form-group">
                    <label class="col-sm-3 control-label">Nome Serviço</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="nomeServico" placeholder="Nome Serviço">
                  </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Endereco</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="enderecoServico" placeholder="Endereço">
                  </div>
                  <label class="col-sm-2 control-label">Numero</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" name="numeroServico" placeholder="Num">
                  </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Complemento</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" name="complementoServico" placeholder="Complemento">
                  </div>
                  <label class="col-sm-2 control-label">Bairro</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" name="bairroServico" placeholder="Bairro">
                  </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Cidade</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="cidadeServico" placeholder="Cidade">
                  </div>
                  <label class="col-sm-1 control-label">Estado</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" name="estadoServico" placeholder="UF">
                  </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Celular</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" name="celularServico" placeholder="(xx)x xxxx-xxxx">
                  </div>
                  <label class="col-sm-3 control-label">Fixo</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" name="fixoServico" placeholder="(xx)x xxxx-xxxx">
                  </div>
                  </div>
                  
                  <div class="form-group">
                    <label  class="col-sm-3 control-label">Data Encerramento</label>
       
                     <div class="col-sm-4">
                       <div class="col-sm-4">
                         <input type="text" class="form-control" name="diaFimServico" placeholder="D">
                       </div>

                       <div class="col-sm-4">
                         <input type="text" class="form-control" name="mesFimServico" placeholder="M">
                       </div>

                       <div class="col-sm-4">
                         <input type="text" class="form-control" name="anoFimServico" placeholder="A">
                       </div>
                     </div>


                  <label class="col-sm-3 control-label">Valor Serviço</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" name="valorServico" placeholder="R$">
                  </div>
                  </div>
                     
                     <br><br>                 
 
                 <div class="form-group">
                  <div class="col-sm-offset-5 col-sm-10">
                    <button type="submit" class="btn btn-default">Enviar</button>
                    <button type="reset" class="btn btn-default">Apagar</button>
                  </div>
                  </div>
                </form>
              


              </div>  
            </div>

       </div> 
     </div>
   </div> 
   
  

 </body>
</html>