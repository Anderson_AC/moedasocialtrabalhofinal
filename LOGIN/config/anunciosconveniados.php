<?php

  include("conn.php");

  //cria a instrução SQL que vai selecionar os dados
  $query = sprintf("SELECT * FROM produto order by nomeProduto");

  //executa a query
  $dados = mysqli_query($conn, $query);

  //transforma os dados em uma lista
  $linha = mysqli_fetch_assoc( $dados);

  //calcula o total de registro
  $total = mysqli_num_rows( $dados);
?>

<html>
  <head>
	  <title>Anuncios de conveniados</title>
	  <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
    <script type="text/javascript" src="http://google.com.br"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.2.1.min.js">
    </script>
    <meta name="text:Search Label" content="" />
    <link href="config/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
 <div id="cadastro">
      <div class="modal-body">
        <div class="content">
          <div id="tudo">
            <div id="cabecalho">
              <h1>Anuncios de conveniados</h1>
              <a href="paginainicial.php"><h2>voltar</h2></a>
              <!-- Formulario da caixa de pesquisa -->
              <form onsubmit="return tagSearch(this)">
                <input type="text" name="tag" value="Pesquisar" 
                onfocus="if (this.value == '{text:Search Label}') {
                this.value=''
                }" 
                onblur="if (this.value == '') {
                this.value='{text:Search Label}'
                }" />
                <input type="submit" value="Pesquisar" />
              </form>
              <div id="menu">
                <ul id="nav">
                  <li>
                    <a href="login.php">Sair</a>
                  </li>
                  <li>
                    <a href="fundada2.php">Sobre</a>
                  </li>
                  <li>
                    <a href="ideia2.php">Ideia</a>
                  </li>
                  <li> 
                    <a href="contato2.php">Contato</a>
                  </li>
                  <li>
                    <a href="mostrarofertas.php">Ofertas</a>
                  </li>
                  <li>
                    <a href="pagamento.php">Pagamentos</a>
                  </li>
                  <li>
                    <a href="anunciosconveniados.php">Anuncios de Conveniados</a>
                  </li>
                  <li>
                    <a href="lojasconveniadas.php">Lojas</a>
                  </li>
                  <li><a href="corpo1.php">Cadastros</a>
                    <ul>
                      <li>
                        <a href="cadastroServico.php">Serviços</a>
                      </li>
                      <li>
                        <a href="cadastromaodeobra.php">Mão de obra</a>
                      </li> </ul>
              </div>
              <br>
              <br>
              <?php
                if ($total > 0) 
                {
  	            // inicia o loop que vai mostrar os dados
                  do
                  {
              ?>  	    	
  	    	          <p> 
                      <?=$linha['nomeProduto']?> / 
                      <?=$linha['valor']?>
                   </p>
              <?php 	    	
  	              }while($linha = mysqli_fetch_assoc($dados)); 
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>